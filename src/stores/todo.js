
import {defineStore} from 'pinia';
import { ref, computed } from 'vue';



export const useTodoStore = defineStore('todo', () =>{
    const todos = ref([
        {
            tod_id: 1,
            tod_task: "prvi_task",
            tod_done: false,
        },
        {
            tod_id: 2,
            tod_task: "drugi_task",
            tod_done: false
        }
    ]);
    let todos_id = 3;
    
    const todo_edit_id = ref(null);
    const isEditing = computed(() => todo_edit_id.value !== null);
    const get_edit_id = computed(() => todo_edit_id.value);
    const editovanje_je_zavrseno = () => todo_edit_id.value = null;

    function dodaj_task(text_taska){
        const task_za_dodavanje = {
            tod_task: text_taska,
            tod_id: todos_id++,
            tod_done: false
        };
    
        todos.value.unshift(task_za_dodavanje);
    }
    function obrisi_task(id){
        todos.value = todos.value.filter(task => task.tod_id !== id);
    }
    function odvrsi_zavrsi(id){
        todos.value.filter(task => task.tod_id === id)
    .forEach(task => task.tod_done = !task.tod_done);
    }

    function edit_task(id){
        todo_edit_id.value = id;
    }

    function zavrsi_edit(novi_tekst){
        todos.value.filter(task => task.tod_id === todo_edit_id.value)
        .forEach(task => task.tod_task = novi_tekst);
        todo_edit_id.value = null;
    }

    return {
        isEditing, 
        get_edit_id,
        editovanje_je_zavrseno,
        todos,
        dodaj_task,
        obrisi_task,
        odvrsi_zavrsi,
        edit_task,
        zavrsi_edit
    }
});